package com.example.queryBuilder.service;

import com.example.queryBuilder.dao.entity.BiometricData;
import com.example.queryBuilder.utils.BiometricData_;

import java.util.List;
import java.util.Map;

public interface BiometricDataService {

    List<BiometricData> getAll();
    Map<String, Object> getGroupBy(List<BiometricData_.Dimensions> dimensions, List<BiometricData_.Measures> measures);
    Map<String, Object> getGroupBy(String[] dimensions, String[] measures);
}
