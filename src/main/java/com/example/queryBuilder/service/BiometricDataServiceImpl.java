package com.example.queryBuilder.service;

import com.example.queryBuilder.dao.BiometricDataRepository;
import com.example.queryBuilder.dao.entity.BiometricData;
import com.example.queryBuilder.utils.BiometricData_;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BiometricDataServiceImpl implements BiometricDataService{

    private final BiometricDataRepository biometricDataRepository;

    public List<BiometricData> getAll() {
        return biometricDataRepository.findAll();
    }

    public Map<String, Object> getGroupBy(List<BiometricData_.Dimensions> dimensions, List<BiometricData_.Measures> measures) {
        List<Object[]> rawData = biometricDataRepository.getSumGroupBy(dimensions, measures);

        Map<String, Object> data = rawToMap(rawData, dimensions.size());
        calculateRollups(data);

        return data;
    }

    private Map<String, Object> rawToMap(List<Object[]> rawData, int dimensionsCount) {
        HashMap<String, Object> root = new HashMap<>();

        for (Object[] row : rawData) {
            List<Object> measures = Arrays.asList(row).subList(dimensionsCount, row.length);

            Map<String, Object> node = root;

            for (int level = 0; level < dimensionsCount-1; level++) {
                if (!node.containsKey(row[level].toString()))
                    node.put(row[level].toString(), new HashMap<String, Object>());

                node = (Map<String, Object>)node.get(row[level].toString());
            }

            node.put(row[dimensionsCount-1].toString(), measures);
        }

        return root;
    }

    private void calculateRollups(Map<String, Object> map) {

        BigDecimal total = new BigDecimal(0);

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (entry.getValue() instanceof Map) {
                Map<String, Object> inner = (Map<String, Object>)entry.getValue();

                calculateRollups(inner);

                BigDecimal innerTotal = (BigDecimal)inner.get("total");
                total = total.add(innerTotal);
            }

            if (entry.getValue() instanceof List) {
                List<BigDecimal> numbers = (List<BigDecimal>)entry.getValue();
                BigDecimal sum = numbers.stream().reduce((acc, r) -> acc = acc.add(r)).get();

                Map<String, Object> newThing = new HashMap<>();
                newThing.put("data", numbers);
                newThing.put("total", sum);
                map.put(entry.getKey(), newThing);
                total = total.add(sum);
            }
        }

        map.put("total", total);
    }

    @Override
    public Map<String, Object> getGroupBy(String[] dimensions, String[] measures) { // TODO: add validation
        List<BiometricData_.Dimensions> properDimensions = Arrays.stream(dimensions).map(BiometricData_.Dimensions::get).collect(Collectors.toList());
        List<BiometricData_.Measures> properMeasures = Arrays.stream(measures).map(BiometricData_.Measures::get).collect(Collectors.toList());

        return getGroupBy(properDimensions, properMeasures);
    }

    /**
     * Function to make sure requested dimensions are valid
     * @return first invalid dimension, if none - null
     */
    public String validateDimensions(String[] potentialDimensions) {
        for (String whoKnowsWhat : potentialDimensions) {
            if (!BiometricData_.Dimensions.contains(whoKnowsWhat))
                return whoKnowsWhat;
        }

        return null;
    }

    /**
     * Works the same way as {@link BiometricDataServiceImpl#validateDimensions(String[])}
     * @return first invalid dimension, if none - null
     */
    public String validateMeasures(String[] potentialMeasures) {
        for (String whoKnowsWhat : potentialMeasures) {
            if (!BiometricData_.Measures.contains(whoKnowsWhat))
                return whoKnowsWhat;
        }

        return null;
    }
}
