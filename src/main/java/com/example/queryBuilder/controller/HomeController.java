package com.example.queryBuilder.controller;

import com.example.queryBuilder.service.BiometricDataServiceImpl;
import com.example.queryBuilder.utils.BiometricData_;
import lombok.RequiredArgsConstructor;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class HomeController {
    private final BiometricDataServiceImpl biometricDataService;

    @GetMapping("/")
    public Map<String, Object> hello(Model model) {
        Map<String, Object> payload = new HashMap<>();

        payload.put("everything", biometricDataService.getAll());

        payload.put("dimensions", BiometricData_.Dimensions.values());
        payload.put("measures", BiometricData_.Measures.values());

        return payload;
    }

    @Deprecated
    @GetMapping("/group")
    public Map<String, Object> group() {
        Map<String, Object> payload = new HashMap<>();

        List<BiometricData_.Dimensions> fieldsToGroup = Arrays.asList(
                BiometricData_.Dimensions.period,
                BiometricData_.Dimensions.clientBiometricFlag,
                BiometricData_.Dimensions.operationBiometricFlag
        );

        List<BiometricData_.Measures> fieldsToShow = Arrays.asList(
                BiometricData_.Measures.operationInAmount,
                BiometricData_.Measures.operationOutAmount
        );

        payload.put("report", biometricDataService.getGroupBy(fieldsToGroup, fieldsToShow));

        return payload;
    }

    // Usage example: localhost:8080/report?dimensions=period,region&measures=operationInAmount,operationOutAmount
    @GetMapping("/report")
    public Map<String, Object> getReport(@RequestParam("dimensions") String[] dimensions, @RequestParam("measures") String[] measures) {
        Map<String, Object> payload = new HashMap<>();

        if (dimensions == null || measures == null) {
            payload.put("error", "Should pass at least one dimension and measure");
            return payload;
        }

        String invalidField = biometricDataService.validateDimensions(dimensions); // TODO: fix validation
        if (invalidField != null) {
            payload.put("error", "Invalid dimension: "+invalidField);
            return payload;
        }

        invalidField = biometricDataService.validateMeasures(measures);
        if (invalidField != null) {
            payload.put("error", "Invalid measure: "+invalidField);
            return payload;
        }

        payload.put("report", biometricDataService.getGroupBy(dimensions, measures));
        return payload;
    }


}
