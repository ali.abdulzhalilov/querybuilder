package com.example.queryBuilder.utils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalDate;
import java.time.MonthDay;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

@Converter(autoApply = true)
public class DateToStringConverter implements AttributeConverter<LocalDate, String> {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM-yyyy");

    @Override
    public String convertToDatabaseColumn(LocalDate localDate) {
        return localDate.format(dateTimeFormatter);
    }

    @Override
    public LocalDate convertToEntityAttribute(String s) {
        return YearMonth.parse(s, dateTimeFormatter).atDay(1);
    }
}
