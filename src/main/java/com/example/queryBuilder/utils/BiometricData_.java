package com.example.queryBuilder.utils;

public class BiometricData_ {
    public static final String period = "period";
    public static final String region                 = "region";
    public static final String productCategory        = "productCategory";
    public static final String operationType          = "operationType";
    public static final String channelTypeCode        = "channelTypeCode";
    public static final String ageCategory            = "ageCategory";
    public static final String gender                 = "gender";
    public static final String educationType          = "educationType";
    public static final String jobType                = "jobType";
    public static final String marriageStatus         = "marriageStatus";
    public static final String clientBiometricFlag    = "clientBiometricFlag";
    public static final String operationBiometricFlag = "operationBiometricFlag";

    public static final String operationInAmount    = "operationInAmount";
    public static final String operationOutAmount   = "operationOutAmount";
    public static final String operationInQuantity  = "operationInQuantity";
    public static final String operationOutQuantity = "operationOutQuantity";

    public enum Dimensions {
        period(BiometricData_.period),
        region(BiometricData_.region),
        productCategory(BiometricData_.productCategory),
        operationType(BiometricData_.operationType),
        channelTypeCode(BiometricData_.channelTypeCode),
        ageCategory(BiometricData_.ageCategory),
        gender(BiometricData_.gender),
        educationType(BiometricData_.educationType),
        jobType(BiometricData_.jobType),
        marriageStatus(BiometricData_.marriageStatus),
        clientBiometricFlag(BiometricData_.clientBiometricFlag),
        operationBiometricFlag(BiometricData_.operationBiometricFlag);

        private final String fieldName;

        Dimensions(final String fieldName) {
            this.fieldName = fieldName;
        }

        @Override
        public String toString() {
            return fieldName;
        }

        public static boolean contains(String potentialField) {
            return get(potentialField) != null;
        }

        public static Dimensions get(String input) {
            for (Dimensions dimension : Dimensions.values()) {
                if (dimension.toString().equals(input)) {
                    return dimension;
                }
            }

            return null;
        }
    }

    public enum Measures{
        operationInAmount(BiometricData_.operationInAmount),
        operationOutAmount(BiometricData_.operationOutAmount),
        operationInQuantity(BiometricData_.operationInQuantity),
        operationOutQuantity(BiometricData_.operationOutQuantity);

        private final String fieldName;

        Measures(final String fieldName) {
            this.fieldName = fieldName;
        }

        @Override
        public String toString() {
            return fieldName;
        }

        public static boolean contains(String potentialField) {
            return get(potentialField) != null;
        }

        public static Measures get(String input) {
            for (Measures measure : Measures.values()) {
                if (measure.toString().equals(input)) {
                    return measure;
                }
            }

            return null;
        }
    }
}