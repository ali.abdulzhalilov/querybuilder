package com.example.queryBuilder.dao.entity;

import com.example.queryBuilder.utils.BiometricData_;
import com.example.queryBuilder.utils.DateToStringConverter;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@Data
@Table(name = "biometric_data")
public class BiometricData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_data_sequence")
    @SequenceGenerator(
            name = "pk_data_sequence",
            sequenceName = "data_id_seq",
            allocationSize = 1)
    private Long id;
    @Column(name = BiometricData_.period)
    @Convert(converter = DateToStringConverter.class)
    private LocalDate period;

    @Column(name = BiometricData_.region)
    private Long region;
    @Column(name = BiometricData_.productCategory)
    private Long productCategory;
    @Column(name = BiometricData_.operationType)
    private Long operationType;
    @Column(name = BiometricData_.channelTypeCode)
    private Long channelTypeCode;
    @Column(name = BiometricData_.ageCategory)
    private Long ageCategory;
    @Column(name = BiometricData_.gender)
    private Long gender;
    @Column(name = BiometricData_.educationType)
    private Long educationType;
    @Column(name = BiometricData_.jobType)
    private Long jobType;
    @Column(name = BiometricData_.marriageStatus)
    private Long marriageStatus;
    @Column(name = BiometricData_.clientBiometricFlag)
    private Long clientBiometricFlag;
    @Column(name = BiometricData_.operationBiometricFlag)
    private Long operationBiometricFlag;

    @Column(name = BiometricData_.operationInAmount)
    private BigDecimal operationInAmount;
    @Column(name = BiometricData_.operationOutAmount)
    private BigDecimal operationOutAmount;
    @Column(name = BiometricData_.operationInQuantity)
    private Long operationInQuantity;
    @Column(name = BiometricData_.operationOutQuantity)
    private Long operationOutQuantity;
}
