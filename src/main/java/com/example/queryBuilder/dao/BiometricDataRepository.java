package com.example.queryBuilder.dao;

import com.example.queryBuilder.dao.entity.BiometricData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BiometricDataRepository extends JpaRepository<BiometricData, Long>, BiometricDataRepositoryCustom {
}

