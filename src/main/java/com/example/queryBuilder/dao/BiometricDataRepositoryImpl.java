package com.example.queryBuilder.dao;

import com.example.queryBuilder.dao.entity.BiometricData;
import com.example.queryBuilder.utils.BiometricData_;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class BiometricDataRepositoryImpl implements BiometricDataRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Object[]> getSumGroupBy(List<BiometricData_.Dimensions> fieldToGroupBy, List<BiometricData_.Measures> fieldToSumUp) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object[]> query = cb.createQuery(Object[].class);

        Root<BiometricData> data = query.from(BiometricData.class);

        List<Expression<?>> groupingExpressions = fieldToGroupBy.stream().map(m -> data.get(m.toString())).collect(Collectors.toList());
        query.groupBy(groupingExpressions);

        List<Selection<?>> groupingSelections = new ArrayList<>(groupingExpressions);
        fieldToSumUp.forEach(m -> groupingSelections.add(cb.sum(data.get(m.toString()))));
        query.multiselect(groupingSelections);

        TypedQuery<Object[]> typedQuery = em.createQuery(query);
        return typedQuery.getResultList();
    }
}
