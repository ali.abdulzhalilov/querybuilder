package com.example.queryBuilder.dao;

import com.example.queryBuilder.utils.BiometricData_;

import java.util.List;

public interface BiometricDataRepositoryCustom {
    List<Object[]> getSumGroupBy(List<BiometricData_.Dimensions> fieldToGroupBy, List<BiometricData_.Measures> fieldToSumUp);
}
